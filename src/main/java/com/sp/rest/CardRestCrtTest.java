package com.sp.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sp.model.Card;
import com.sp.service.CardService;
import model.Family;
import org.h2.engine.User;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(CardRestCrt.class)
class CardRestCrtTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private CardRestCrt controller;
    @MockBean
    private CardService service;

    @Mock
    private Card mockCard;
    private Card card;
    @Mock
    private User mockUser;

    private ObjectMapper obj = new ObjectMapper();

    private String jsonCard = "{id:'1', name:'testCard', description:'this is a card made for tests', image_url:'https://upload.wikimedia.org/wikipedia/commons/2/20/Point_d_interrogation.jpg', affinity:'Air', family:'COMMON', energy:'100', attack:'100', defense:'100', hp:'100'}";
    private Card testcard = new Card(1, "TagsCard", "card with more than one tag", "", Family.MYSTICAL, "Feu", 100, 100, 100, 100, 100, "tag1, tag2");

    @Before
    public void setUp(){
        MockitoAnnotations.initMocks(this);
        this.card = this.mockCard;
    }


    @Test
    public void contextLoads() throws Exception{
        assertThat(controller).isNotNull();
    }

    @Test
    public void getCards() throws Exception {
        when(service.getCards()).thenReturn(new ArrayList<Card>());
        this.mockMvc.perform(get("/api/cards")).andDo(print()).andExpect(status().isOk()).andExpect((content().string(containsString("[]"))));
        // database is empty
    }

    @Test
    void addCard() throws Exception{
        when(service.addCard(this.card)).thenReturn(true);
        this.mockMvc.perform(post("/api/cards").contentType(MediaType.APPLICATION_JSON_UTF8_VALUE).content(obj.writeValueAsString(testcard)))
                .andDo(print()).andExpect(status().isOk()).andExpect(content().string(containsString("false")));
    }

    @Test
    void addCardWhenEmpty() throws Exception {
        when(service.addCard(null)).thenReturn(false);
        this.mockMvc.perform(post("/api/cards").contentType(MediaType.APPLICATION_JSON))
                .andDo(print()).andExpect(status().isBadRequest());
    }

    @Test
    void addCardToUser() throws Exception {
        when(service.addCardToUser(testcard.getId(), mockUser.getId())).thenReturn(true);
        this.mockMvc.perform(post("/api/cards/" + testcard.getId() + "?uid=" + mockUser.getId()).contentType(MediaType.APPLICATION_JSON))
                .andDo(print()).andExpect(status().isOk()).andExpect(content().string(containsString("false")));
    }

    @Test
    void getCard() throws Exception {
        when(service.getCardById(testcard.getId())).thenReturn(new Card());
        this.mockMvc.perform(get("/api/cards/" + testcard.getId())).andDo(print()).andExpect(status().isOk()).andExpect((content().string(containsString("id"))));
    }

    @Test
    void deleteCardFromUser() throws Exception {
        when(service.removeCardToUser(testcard.getId(), mockUser.getId())).thenReturn(true);
        this.mockMvc.perform(delete("/api/cards/" + testcard.getId() + "?uid=" + mockUser.getId())).andDo(print()).andExpect(status().isOk()).andExpect(content().string(containsString("false")));
    }

    @Test
    void deleteCardFromShop() throws Exception {
        when(service.deleteCard(testcard.getId())).thenReturn(true);
        this.mockMvc.perform(delete("/api/cards/" + mockCard.getId())).andDo(print()).andExpect(status().isOk()).andExpect(content().string(containsString("false")));
    }

    @Test
    void updateCard() throws Exception {
        when(service.updateCard(mockUser.getId(), testcard)).thenReturn(true);
        this.mockMvc.perform(put("/api/cards/" + mockUser.getId()).contentType(MediaType.APPLICATION_JSON).content(obj.writeValueAsString(testcard)))
                .andDo(print()).andExpect(status().isOk()).andExpect(content().string(containsString("false")));
    }
}