package com.sp.rest;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.sp.model.Card;
import com.sp.service.CardService;

import model.UserDTO;
@CrossOrigin
@RestController
@RequestMapping("/api/cards")
public class CardRestCrt {
    @Autowired
    CardService cardService;

    /*
     * GET /cards?uid=uid
     */
    @RequestMapping(method = RequestMethod.GET, value = "")
    public ArrayList<Card> getCards(@RequestParam(required = false) String uid) {
        if (uid == null) {
            ArrayList<Card> cards = cardService.getCards();
            return cards;
        } else {
            ArrayList<Card> cards = cardService.getCardsByUser(Long.parseLong(uid));
            return cards;
        }
    }
    
   

    /*
     * POST /cards
     */
    @RequestMapping(method = RequestMethod.POST, value = "")
    public boolean addCard(@RequestBody Card card) {
        return cardService.addCard(card);
    }

    /*
     * POST /cards/:cid?uid=uid
     */
    @RequestMapping(method=RequestMethod.POST,value="/{cid}")
    public boolean addCardToUser(@PathVariable String cid ,@RequestParam() String uid) {
        return cardService.addCardToUser(Long.parseLong(uid), Long.parseLong(cid));
    }

    /*
     * GET /cards/:id
     */
    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    public Card getCard(@PathVariable String id) {
        Card card = cardService.getCardById(Long.valueOf(id));
        return card;
    }

    /*
     * DELETE /cards/:id?uid=uid
     */
    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
    public boolean deleteCard(@PathVariable String id, @RequestParam(required = false) String uid) {
        if(uid == null)
            return cardService.deleteCard(Long.valueOf(id));
        else
            return cardService.removeCardToUser(Long.valueOf(uid),Long.valueOf(id));
    }

    /*
     * PUT /cards/:id
     */
    @RequestMapping(method=RequestMethod.PUT,value="/{id}")
    public boolean updateCard(@PathVariable String id, @RequestBody Card card) {
        return cardService.updateCard(Long.valueOf(id), card);
    }
}
