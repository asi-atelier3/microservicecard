package com.sp.repository;

import com.sp.model.Card;
import com.sp.model.Inventory;
import com.sp.model.InventoryId;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface InventoryRepository extends JpaRepository<Inventory, Integer> {
    public List<Inventory> findAll();
    public Inventory findById(InventoryId id);
    @Query(value = "SELECT i FROM Inventory i WHERE i.id.cid = ?1")
    public List<Inventory> findByCard(long cid);
    @Query(value = "SELECT i FROM Inventory i WHERE i.id.uid = ?1")
    public List<Inventory> findByUser(long uid);
    public Inventory save(Inventory inventory);

    public void delete(Inventory inventory);
}