package com.sp.repository;
import java.util.List;

import model.Family;
import org.springframework.data.repository.CrudRepository;

import com.sp.model.Card;

public interface CardRepository extends CrudRepository<Card, Integer> {

	public List<Card> findAll();

	public Card findById(long id);

	public Card save(Card card);

	public void delete(Card card);

	public Card findByName(String name);

	public List<Card> findByFamily(Family family);
}