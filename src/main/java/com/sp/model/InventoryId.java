package com.sp.model;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;

public class InventoryId implements Serializable{
    @Column(name = "user_id")
    private long uid;

    @Column(name = "card_id")
    private long cid;


    public long getUid() {
        return this.uid;
    }

    public void setUid(long uid) {
        this.uid = uid;
    }

    public long getCid() {
        return this.cid;
    }

    public void setCid(long cid) {
        this.cid = cid;
    }

    public InventoryId() {
    }

    public InventoryId(long uid, long cid) {
        this.uid = uid;
        this.cid = cid;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof InventoryId)) {
            return false;
        }
        InventoryId inventoryId = (InventoryId) o;
        return uid == inventoryId.uid && cid == inventoryId.cid;
    }

    @Override
    public int hashCode() {
        return Objects.hash(uid, cid);
    }
}