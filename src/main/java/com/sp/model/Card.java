package com.sp.model;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import model.Family;

@Entity
public class Card {
	@Id
	@GeneratedValue
	private long id;
	private String name, description, image_url, affinity, tags;
	@Enumerated(EnumType.STRING)
	private Family family;
	private long energy, attack, defense, hp, price;

	public Card() {

	}

	public Card(int id, String name, String description, String image_url, Family family, String affinity, long hp,
			long energy, long attack, long defense, long price, String tags) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.image_url = image_url;
		this.family = family;
		this.affinity = affinity;
		this.hp = hp;
		this.energy = energy;
		this.attack = attack;
		this.defense = defense;
		this.price = price;
		this.tags = tags;
	}
	
	public String getTags() {
		return this.tags;
	}
	
	public void setTags(String tags) {
		this.tags = tags;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getPrice() {
		return this.price;
	}

	public void setPrice(long price) {
		this.price = price;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImage_url() {
		return image_url;
	}

	public void setImage_url(String image_url) {
		this.image_url = image_url;
	}

	public Family getFamily() {
		return family;
	}

	public void setFamily(Family family) {
		this.family = family;
	}

	public String getAffinity() {
		return affinity;
	}

	public void setAffinity(String affinity) {
		this.affinity = affinity;
	}

	public long getHp() {
		return hp;
	}

	public void setHp(long hp) {
		this.hp = hp;
	}

	public long getEnergy() {
		return energy;
	}

	public void setEnergy(long energy) {
		this.energy = energy;
	}

	public long getAttack() {
		return attack;
	}

	public void setAttack(long attack) {
		this.attack = attack;
	}

	public long getDefense() {
		return defense;
	}

	public void setDefense(long defense) {
		this.defense = defense;
	}

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "{" +
				"id='" + getId() + "'" +
				", name='" + getName() + "'" +
				", description='" + getDescription() + "'" +
				", image_url='" + getImage_url() + "'" +
				", affinity='" + getAffinity() + "'" +
				", family='" + getFamily() + "'" +
				", energy='" + getEnergy() + "'" +
				", attack='" + getAttack() + "'" +
				", defense='" + getDefense() + "'" +
				", hp='" + getHp() + "'" +
				"}";
	}

}
