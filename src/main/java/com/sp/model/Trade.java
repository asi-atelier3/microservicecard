package com.sp.model;

public interface Trade {
    public boolean buy(Card card);
    public boolean sell(Item item);
}