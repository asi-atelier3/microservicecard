package com.sp.service;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.List;

import model.Family;
import com.sp.model.Inventory;
import com.sp.model.InventoryId;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sp.model.ApiURL;
import com.sp.model.Card;
import com.sp.repository.CardRepository;
import com.sp.repository.InventoryRepository;

import model.UserDTO;

@Service
public class CardService {
	@Autowired
	CardRepository cardRepository;
	@Autowired
	InventoryRepository inventoryRepository;
	
	public boolean addCard(Card card) {
		try {			
			String urlJson ="{" +
							"\"img\":\"" + card.getImage_url() +"\""
							+ "}";
			// On récupère les tags de l'image
			HttpRequest request = HttpRequest.newBuilder().uri(URI.create(ApiURL.TagsAPI)).header("Content-Type", "application/json").POST(HttpRequest.BodyPublishers.ofString(urlJson)).build();
			HttpClient client = HttpClient.newHttpClient();
	        HttpResponse response = client.send(request, HttpResponse.BodyHandlers.ofString());
	        String tags = response.body().toString();
	        if(response.statusCode() == 200) {
	        	card.setTags(tags);
	        	cardRepository.save(card);
				return true;
	        	
	        }
	        return false;			
		} catch (Exception e) {
			return false;
		}
	}

	public boolean addCardToUser(long uid, long cid) {
		try {			
			if(cardRepository.findById(cid) == null ) return false; 
			
			// On récupère l'utilisateur avec un DTO et on vérifie s'il existe
			HttpRequest request = HttpRequest.newBuilder().uri(URI.create(ApiURL.UserAPI + "/" + uid)).header("Content-Type", "application/json").GET().build();
			HttpClient client = HttpClient.newHttpClient();
	        HttpResponse response = client.send(request, HttpResponse.BodyHandlers.ofString());
	        
	        if(response.statusCode() == 200) {
	        	ObjectMapper objectMapper = new ObjectMapper();
		        UserDTO user = objectMapper.readValue(response.body().toString(), UserDTO.class);
		        Card card = cardRepository.findById(cid);
		        
				if(user.getMoney() > card.getPrice()) {
					
					Inventory inventory = inventoryRepository.findById(new InventoryId(uid, cid));
					if (inventory == null) {
						inventory = new Inventory(uid, cid, 1);
						System.out.println(inventoryRepository.save(inventory));
					} else {
						inventory.setQuantity(inventory.getQuantity() + 1);
						inventoryRepository.save(inventory);
					}
					
					long money = user.getMoney()-card.getPrice();
					user.setMoney((int) money);
					
					// On modifie l'argent que possède l'utilisateur
					request = HttpRequest.newBuilder().uri(URI.create(ApiURL.UserAPI + "/" + uid)).header("Content-Type", "application/json").PUT(HttpRequest.BodyPublishers.ofString(user.toString())).build();
					client = HttpClient.newHttpClient();
			        response = client.send(request, HttpResponse.BodyHandlers.ofString());
			        
			        if(response.statusCode() == 200)return true;
			        else return false;
				}      	
	        }else {
	        	return false; 
	        }
	        
		} catch (Exception e) {
			System.out.println(e);
			return false;
		}
		return false;
	}

	public boolean removeCardToUser(long uid, long cid) {
		try {
			
			if(cardRepository.findById(cid) == null ) return false; 

			// On récupère l'utilisateur avec un DTO et on vérifie s'il existe
			HttpRequest request = HttpRequest.newBuilder().uri(URI.create(ApiURL.UserAPI + "/" + uid)).header("Content-Type", "application/json").GET().build();
			HttpClient client = HttpClient.newHttpClient();
	        HttpResponse response = client.send(request, HttpResponse.BodyHandlers.ofString());
	        
	        if(response.statusCode() == 200) {
	        	ObjectMapper objectMapper = new ObjectMapper();
		        UserDTO user = objectMapper.readValue(response.body().toString(), UserDTO.class);
		        Card card = cardRepository.findById(cid);
		        
		        
		        Inventory inventory = inventoryRepository.findById(new InventoryId(uid, cid));
				if (inventory == null)
					return false;
				else {
					inventory.setQuantity(inventory.getQuantity() - 1);
					if (inventory.getQuantity() > 0)
						inventoryRepository.save(inventory);
					else
						inventoryRepository.delete(inventory);
				}				
				long money = user.getMoney()+card.getPrice();
				user.setMoney((int) money);
				
				// On modifie l'argent que possède l'utilisateur
				request = HttpRequest.newBuilder().uri(URI.create(ApiURL.UserAPI + "/" + uid)).header("Content-Type", "application/json").PUT(HttpRequest.BodyPublishers.ofString(user.toString())).build();
				client = HttpClient.newHttpClient();
		        response = client.send(request, HttpResponse.BodyHandlers.ofString());
		        
		        if(response.statusCode() == 200)return true;
		        else return false;
		        
		        
		        
		        
	        }
	        return false; 
	        
	        
			
		} catch (Exception e) {
			System.out.println(e);
			return false;
		}
	}

	public ArrayList<Card> getCards() {
		ArrayList<Card> cards = new ArrayList<>();
		Iterable<Card> itCard = cardRepository.findAll();
		for (Card card : itCard) {
			cards.add(card);
		}
		return cards;
	}

	public ArrayList<Card> getCardsByUser(long uid) {
		ArrayList<Card> cards = new ArrayList<>();
		Iterable<Inventory> itCard = inventoryRepository.findByUser(uid);
		for (Inventory inv : itCard) {
			System.out.println(inv);
			for (int i = 0; i < inv.getQuantity(); i++) {
				cards.add(cardRepository.findById(inv.getId().getCid()));
			}
		}
		return cards;
	}

	public Card getCardById(long id) {
		return cardRepository.findById(id);
	}

	public Card getCardByName(String name) {
		return cardRepository.findByName(name);
	}

	public List<Card> getCardsByFamily(Family family) {
		return cardRepository.findByFamily(family);
	}

	public boolean deleteCard(long id) {
		try {
			cardRepository.delete(getCardById(id));
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public boolean updateCard(long id, Card card) {
		try {
			card.setId(id);
			cardRepository.save(card);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
}